import subprocess
import os

manual = []

# Set path to current directory
path = os.getcwd()
sums_path = path + '/human_sums.txt'


def check_prior(file):
	prior = str(subprocess.check_output([f'find . -name {file}'], shell=True))
	if file in prior:
		return True
	else:
		return False


def reference_check(i):
	with open(sums_path, 'r') as f:
		if i in f.read():
			print('File successfully validated: ' + str(i).rsplit(' ', 1)[1])
			return True
		else:
			print('File failed validation: ' + str(i).rsplit(' ', 1)[1])
			return False


def download_reference(cmd, alt=None):
	if alt == None:
		file = cmd.rsplit('/', 1)[1]
	else:
		file = alt
	attempt = 0
#   while True:
#		if attempt == 3:
#			manual.append(f'{file}')
#			return False
	subprocess.run([f'{cmd}'], shell=True)
#		check = str(subprocess.check_output([f'md5sum {file}'], shell=True)).split("'", 1)[1].rsplit("\\", 1)[0]
#		if reference_check(check):
	return True
#		else:
#			subprocess.run([f'rm {file}'], shell=True)
#			attempt += 1


### Genomic reference ###

subprocess.run([f'mkdir -p {path}/references/homo_sapiens'], shell=True)
os.chdir(f'{path}/references/homo_sapiens')

if not check_prior('Homo_sapiens.assembly38.no_ebv.fa'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/fasta'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/fasta')

	download_reference('wget https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.fasta')
	subprocess.run(['singularity pull docker://staphb/samtools:1.13'], shell=True)
	subprocess.run(['singularity exec -B $PWD samtools*.s* samtools faidx Homo_sapiens_assembly38.fasta'], shell=True)
	subprocess.run(["awk '{print $1}' Homo_sapiens_assembly38.fasta.fai | grep -v chrEBV > keep_ids"], shell=True)
	subprocess.run(["singularity exec -B $PWD samtools*.s* samtools faidx -r keep_ids -o Homo_sapiens.assembly38.no_ebv.fa Homo_sapiens_assembly38.fasta"],
				   shell=True)
	subprocess.run(['rm Homo_sapiens_assembly38.fasta'], shell=True)
	subprocess.run(['rm Homo_sapiens_assembly38.fasta.fai'], shell=True)
	subprocess.run(['rm samtools*.s*'], shell=True)
	subprocess.run(['rm keep_ids'], shell=True)
else:
	print('Found pre-existing file: Homo_sapiens.assembly38.no_ebv.fa')
os.chdir(f'{path}/references/homo_sapiens')


### GTF/GFF3 ###

if not check_prior('gencode.v37.annotation.with.hervs.gtf'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/annot'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/annot')

	download_reference('wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_37/gencode.v37.annotation.gtf.gz')
	download_reference('wget http://geve.med.u-tokai.ac.jp/download_data/gtf_m/Hsap38.geve.m_v1.gtf.bz2')
	subprocess.run(['bzip2 -d Hsap38.geve.m_v1.gtf.bz2'], shell=True)
	subprocess.run(['zcat gencode.v37.annotation.gtf.gz > gencode.v37.annotation.with.hervs.gtf;'], shell=True)
	subprocess.run(["cat Hsap38.geve.m_v1.gtf | sed 's/^/chr/g' | sed 's/CDS/transcript/g' >> gencode.v37.annotation.with.hervs.gtf"], shell=True)
	subprocess.run(["cat Hsap38.geve.m_v1.gtf | sed 's/^/chr/g' | sed 's/CDS/exon/g' >> gencode.v37.annotation.with.hervs.gtf"], shell=True)
else:
	print('Found pre-existing file: gencode.v37.annotation.with.hervs.gtf')
os.chdir(f'{path}/references/homo_sapiens')


if not check_prior('gencode.v37.annotation.gff3'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/annot'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/annot')
	download_reference('wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_37/gencode.v37.annotation.gff3.gz')
	subprocess.run(['gunzip gencode.v37.annotation.gff3.gz'], shell=True)
else:
	print('Found pre-existing file: gencode.v37.annotation.gff3.gz')
os.chdir(f'{path}/references/homo_sapiens')


### Protein reference ###

if not check_prior('gencode.v37.pc_translations.fa'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/protein'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/protein')

	download_reference('wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_37/gencode.v37.pc_translations.fa.gz')
	subprocess.run(['gunzip gencode.v37.pc_translations.fa.gz'], shell=True)
else:
	print('Found pre-existing file: gencode.v37.pc_translations.fa.gz')
os.chdir(f'{path}/references/homo_sapiens')


### VCF references ###

if not check_prior('somalier.sites.hg38.vcf.gz'):
    subprocess.run([f'mkdir -p {path}/references/homo_sapiens/vcfs'], shell=True)
    os.chdir(f'{path}/references/homo_sapiens/vcfs')
    download_reference('https://github.com/brentp/somalier/files/3412456/sites.hg38.vcf.gz')
    subprocess.run(['mv sites.hg38.vcf.gz somalier.sites.hg38.vcf.gz'], shell=True)
    os.chdir(f'{path}/references/homo_sapiens')
else:
    print('Found pre-existing file: somalier.sites.hg38.vcf.gz')

if not check_prior('1000g_pon.hg38.vcf.gz'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/vcfs'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/vcfs')
	download_reference('wget https://storage.googleapis.com/gatk-best-practices/somatic-hg38/1000g_pon.hg38.vcf.gz')
else:
	print('Found pre-existing file: 1000g_pon.hg38.vcf.gz')
os.chdir(f'{path}/references/homo_sapiens')

if not check_prior('af-only-gnomad.hg38.vcf.gz'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/vcfs'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/vcfs')
	download_reference('wget https://storage.googleapis.com/gatk-best-practices/somatic-hg38/af-only-gnomad.hg38.vcf.gz')
else:
	print('Found pre-existing file: af-only-gnomad.hg38.vcf')
os.chdir(f'{path}/references/homo_sapiens')

if not check_prior('Homo_sapiens_assembly38.dbsnp138.vcf.gz'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/vcfs'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/vcfs')
	download_reference('wget https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.dbsnp138.vcf')
	subprocess.run(['singularity pull docker://bioslimcontainers/tabix:1.7'], shell=True)
	subprocess.run(['singularity exec -B $PWD tabix*.s* bgzip Homo_sapiens_assembly38.dbsnp138.vcf'], shell=True)
	subprocess.run(['rm tabix*.s*'], shell=True)
else:
	print('Found pre-existing file: Homo_sapiens_assembly38.dbsnp138.vcf.gz')
os.chdir(f'{path}/references/homo_sapiens')

if not check_prior('small_exac_common_3.hg38.vcf.gz'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/vcfs'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/vcfs')
	download_reference('wget https://storage.googleapis.com/gatk-best-practices/somatic-hg38/small_exac_common_3.hg38.vcf.gz')
else:
	print('Found pre-existing file: small_exac_common_3.hg38.vcf.gz')
os.chdir(f'{path}/references/homo_sapiens')


### BEDs ###

if not check_prior('hg38_exome.bed'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/beds'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/beds')
	subprocess.run([f'wget https://gitlab.com/landscape-of-effective-neoantigens-software/nextflow/modules/tools/lens/-/wikis/uploads/ea7d0b56f0af067ea9e7a3c0e9c96c96/hg38_bed_generator.sh'], shell=True)
	subprocess.run(['bash hg38_bed_generator.sh ../annot/gencode.v37.annotation.gtf.gz'], shell=True)
else:
	print('Found pre-existing file: hg38_exome.bed')
os.chdir(f'{path}/references/homo_sapiens')


### snpEff reference ###

if not check_prior('snpEff.config'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/snpeff'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/snpeff')
	subprocess.run(['singularity pull docker://resolwebio/snpeff:latest'], shell=True)
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/snpeff/GRCh38.GENCODEv37'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/snpeff/GRCh38.GENCODEv37')

	download_reference('wget https://gitlab.com/landscape-of-effective-neoantigens-software/nextflow/modules/tools/lens/-/wikis/uploads/430f9d80c841721499fbcec937b0f721/snpEff.config')
	subprocess.run(['ln ../../annot/gencode.v37.annotation.gtf.gz genes.gtf.gz'], shell=True)
	subprocess.run(['ln ../../fasta/Homo_sapiens.assembly38.no_ebv.fa sequences.fa'], shell=True)
	os.chdir('..')
	subprocess.run(['singularity exec -B $PWD snpeff*.s* /opt/snpeff/snpeff/bin/snpEff build -gtf22 -v GRCh38.GENCODEv37 -dataDir ${PWD} -c GRCh38.GENCODEv37/snpEff.config'],
				   shell=True)
	subprocess.run(['rm GRCh38.GENCODEv37/sequences.fa'], shell=True)
	subprocess.run(['rm GRCh38.GENCODEv37/genes.gtf.gz'], shell=True)
	subprocess.run(['rm snpeff*.s*'], shell=True)
	os.chdir('..')

else:
	print('Found pre-existing file: snpEff.config')
	os.chdir(f'{path}/references/homo_sapiens')


### NeoSplice reference ###

if not check_prior('reference_peptidome_8.txt.gz'):

	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/neosplice'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/neosplice')
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/neosplice/peptidome.homo_sapiens'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/neosplice/peptidome.homo_sapiens')
	download_reference('wget https://github.com/Benjamin-Vincent-Lab/NeoSplice/raw/master/Reference_peptidome/reference_peptidome_8.txt.gz')
	download_reference('wget https://github.com/Benjamin-Vincent-Lab/NeoSplice/raw/master/Reference_peptidome/reference_peptidome_9.txt.gz')
	download_reference('wget https://github.com/Benjamin-Vincent-Lab/NeoSplice/raw/master/Reference_peptidome/reference_peptidome_10.txt.gz')
	download_reference('wget https://github.com/Benjamin-Vincent-Lab/NeoSplice/raw/master/Reference_peptidome/reference_peptidome_11.txt.gz')
else:
	print('Found pre-existing file: reference_peptidome')
os.chdir(f'{path}/references/homo_sapiens')

### SNAF reference ###

if not check_prior('snaf-data'):

    subprocess.run([f'mkdir -p {path}/references/homo_sapiens/snaf'], shell=True)
    os.chdir(f'{path/references/homo_sapiens/snaf')
    download_reference('wget http://altanalyze.org/SNAF/download.tar.gz')
    subprocess.run(['tar -xvf download.tar.gz'], shell=True)
    subprocess.run(['mv download snaf-data'], shell=True)
else:
    print('Found pre-existing file: snaf-data')

### CTA/Self-antigen reference ###

if not check_prior('cta_and_self_antigen.homo_sapiens.gene_list'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/cta_self'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/cta_self')
	download_reference('wget https://gitlab.com/landscape-of-effective-neoantigens-software/nextflow/modules/tools/lens/-/wikis/uploads/5a9786203497b90c0cc0c0a6a251399b/cta_and_self_antigen.homo_sapiens.gene_list')
	download_reference('wget https://gitlab.com/landscape-of-effective-neoantigens-software/nextflow/modules/tools/lens/-/wikis/uploads/773c64236d269a419f63e217c8cfa820/canonical_txs.mtec.norm.subcell.annot.tsv')

else:
	print('Found pre-existing file: cta_and_self_antigen.homo_sapiens.gene_list')
os.chdir(f'{path}/references/homo_sapiens')


### STARFusion reference ###

if not check_prior('starfusion'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/starfusion'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/starfusion')
	download_reference('wget https://data.broadinstitute.org/Trinity/CTAT_RESOURCE_LIB/__genome_libs_StarFv1.10/GRCh38_gencode_v37_CTAT_lib_Mar012021.plug-n-play.tar.gz')
	subprocess.run(['tar -xvf GRCh38_gencode_v37_CTAT_lib_Mar012021.plug-n-play.tar.gz'], shell=True)
	os.chdir('GRCh38_gencode_v37_CTAT_lib_Mar012021.plug-n-play')
	subprocess.run(['mv ctat_genome_lib_build_dir/* .; rm -rf ctat_genome_lib_build_dir/'], shell=True)
	os.chdir('..')
	subprocess.run(['rm -rf GRCh38_gencode_v37_CTAT_lib_Mar012021.plug-n-play.tar.gz'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens')
else:
	print('Found pre-existing directory: starfusion')
os.chdir(f'{path}/references/homo_sapiens')

### ERV reference ###

if not check_prior('Hsap38.txt.bz2'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/erv'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/erv')
	if download_reference('wget http://geve.med.u-tokai.ac.jp/download_data/table/Hsap38.txt.bz2'):
		subprocess.run(['bzip2 -d Hsap38.txt.bz2'], shell=True)
	download_reference('wget https://gitlab.com/landscape-of-effective-neoantigens-software/nextflow/modules/tools/lens/-/wikis/uploads/ad2f3b34ade0b1985ae3f741a2c8bc4d/erv_scores.25SEP2023.tsv')
	os.chdir(f'{path}/references/homo_sapiens')

else:
	print('Found pre-existing file: Hsap38.txt.bz2')


### Show list of files that could not be validated ###

if len(manual) > 0:
	print('These files could not be validated and must be downloaded manually. Please refer to the LENS documentation.')
	for item in manual:
		print(item)
else:
	print('All human references downloaded successfully!')
